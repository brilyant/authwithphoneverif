const express = require("express")
// const path = require("path") 

const bodyParser = require('body-parser')
const mongo = require('mongodb').MongoClient
const moment = require('moment')

const app = express()
const port = "3012"

app.use(bodyParser.json())
app.listen(port, () => {
    console.log(`Listening to requests on http://localhost:${port}`)
}) 

//START - Settingan SMS Sender
const GOSMSSender = require('./GOSMSSender')
const ZenzivaSMSSender = require('./ZenzivaSMSSender')
/*
 Provider gosmsgateway
 ============== CATATAN ============== 
Untuk menggunakan Provider gosmsgateway (Class GOSMSSender) Harap Daftar terlebih dahulu di https://www.gosmsgateway.com/ untuk menggunakan layanan sms gateway nya dan mendapatkan free sms 25 x
Akun di bawah (fachmi) ada kemungkinan bisa kehabisan quota freenya, jadi di harapkan untuk proses development kita registrasi sendiri untuk memanfaatkan 
fitur free smsnya 
*/
// GOSMSSender.Config({
//     servername: 'http://secure.gosmsgateway.com/api', //di dapat dr email setelah register (Gunakan HTTP jangan HTTPS, error dari web mereka bila https)
//     username:'fachmi',  //username akun https://www.gosmsgateway.com/
//     password: 'btu123', //password akun https://www.gosmsgateway.com/
// }) 

/*
Provider Zenziva
============== CATATAN ============== 
Untuk menggunakan Provider Zenziva (Class ZenzivaSMSSender) Harap Daftar terlebih dahulu di https://www.zenziva.id/ untuk menggunakan layanan sms gateway nya dan mendapatkan free sms 10x
Akun di bawah (fachmi) ada kemungkinan bisa kehabisan quota freenya, jadi di harapkan untuk proses development kita registrasi sendiri untuk memanfaatkan fitur free smsnya.
Setelah Register kita di haruskan mengirim dokumen untuk mengaktifkan API nya (bisa di cek sendiri di dashboard zenziva -> API Setting)
*/
ZenzivaSMSSender.Config({
    username:'d16d5167e046',    //username (userkey) ada pada https://gsm.zenziva.net/dashboard/api/ di bagian HTTP API
    password: 'hv6q35jh8u',     //password (API Key) ada pada https://gsm.zenziva.net/dashboard/api/ di bagian HTTP API
}) 

const SMS_OTP_TEMPLATE = (otp) => `Berikut OTP Anda ${otp}. Berlaku selama 5 menit`
const SMS_OTP_RESEND_TEMPLATE = (otp) => `Berikut Pengiliman ulang untuk OTP Anda ${otp}. Berlaku selama 5 menit`
 
//END - Settingan SMS Sender


const url = 'mongodb://localhost:27017'

mongo.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, (err, client) => {
    if (err) {
        console.error(err)
        return
    }
    const db = client.db('DBRegisterPhoneVerif')
    const collectionUser = db.collection('user')

    // app.get("/tes", (req, res) => {  
    //     ZenzivaSMSSender.sendSMS('085223839107', SMS_OTP_TEMPLATE(765811), (error, response) => {
    //         if (error) {
    //             console.log(error)
    //             return res.status(500).send('kirim sms error, cek log')
    //         }
    //         //sms berhasil, cek log nya 
    //         console.log(response)
    //         res.send('cek log')
    //     })
    // })

    //ENDPOINT Register
    app.post("/register", (req, res) => {  
        /*  model yg digunakan:
            {
                name,           :   string
                phone,          :   string
                registertime,   :   Date
                otp,            :   number
                status          :   enum ['Verifying', 'Verified']
            }
        */
        const { name, phone } = req.body
        console.log(name, phone)

        //Cari dlu apakah nama (name) yg di inputkan sudah ada?
        collectionUser.findOne({ name, status: 'Verifying' }, function (err, item) { 
            if (err) {
                console.log(err)
                return res.send('gagal mencari name di db')
            }
            // console.log('hagh', item)
            if (item && getRegisterTimeDurationInMinute(item.registertime) < 5 ) { //cek apakah name ini melakukan register sblmnya, dan masih di bawah 5 menit ?
                //name sudah ada, dan di bawah 5 menit, maka lanjutkan input OTP di mobile, karena belum expired (expirednya 5 menit)
                console.log('masih ada', getRegisterTimeDurationInMinute(item.registertime))

                res.send({isnew: false, message: 'Lanjutkan Input OTP Anda yg dikirim ke nomor '+item.phone, dataobject: item, timetoresend: (3 - getRegisterTimeDurationInMinute(item.registertime)) })
                return
            } else {
                //bila name blm ada, dan meskipun sudah ada tapi di atas 5 menit, asumsikan register baru... timpa record lama (bila ada)
                const otp = generateOtp() 
                console.log('generated otp', name, otp)
                collectionUser.replaceOne(
                    { name, status: 'Verifying' },
                    {name, phone, registertime: new Date(), otp, status: 'Verifying'},
                    {
                        upsert: true, 
                    },
                    function(err, result) {
                        if (err) {
                            return res.status(500).send('Gagal Insert / update Record')
                        } else {
                            //KIRIM SMS
                            ZenzivaSMSSender.sendSMS(
                                phone, SMS_OTP_TEMPLATE(otp), (error, response) => {
                                    if (error) {
                                        console.log(error)
                                        return res.status(500).send('kirim sms error, cek log')
                                    }
                                    //sms berhasil, cek log nya 
                                    console.log(response)
                                }
                            )
                            
                            res.send({isnew: true, message: 'OTP Sedang dikirimkan ke nomor: '+phone, dataobject: result.ops[0], timetoresend: (3 - getRegisterTimeDurationInMinute(result.ops[0].registertime))})
                        }
                    }
                )
            }
              
        })

        
        
    })

    //ENDPOINT Phone Verification
    app.post("/phoneverification", (req, res) => {  
        const { name, otp } = req.body
        collectionUser.findOne({ name, status: 'Verifying', otp }, function (err, item) { 
            if (err) {
                console.log(err)
                return res.send('gagal mencari data di db')
            }
            if (item) {
                //bila status = verifying dan registertime > 5 menit, kasih info otp expired
                if (getRegisterTimeDurationInMinute(item.registertime) > 5) {
                    return res.send({isverified: false, message: 'Kode Telah Kadaluarsa'})
                } else {
                    //update status dari verifying menjadi verified, lalu kirim response sukses
                    collectionUser.findOneAndUpdate(
                        { name },
                        { $set: { status: 'Verified'} },
                        {
                            upsert: true, 
                            returnNewDocument: true
                        },
                        function(err, result) {
                            if (err) {
                                console.log(err)
                                return res.status(500).send('Gagal Insert / update Record')
                            }  
                            res.send({isverified: true, message: 'Berhasil Di Verifikasi'})
                            return
                        }
                    )
                }
                
            } else {
                res.send({isverified: false, message: 'Kode OTP Anda Salah !'})
            }
        })

        //login sukses
    })

    //ENDPOINT Resend OTP
    app.post("/resendotp", (req, res) => {  
        const { name } = req.body
        console.log('name', name)
        //CATATAN, user dapat melakukan resend otp setelah waktu 3 menit.
        collectionUser.findOne({ name, status: 'Verifying' }, function (err, item) { 
            if (err) {
                console.log(err)
                return res.send('gagal mencari data di db')
            }
            if (item) {
                //bila status = verifying dan registertime < 3 menit, generate otp blm memungkinkan, kirim lg existing object d db
                if (getRegisterTimeDurationInMinute(item.registertime) < 3) {
                    console.log('resend otp blm memungkinkan')
                    res.send({isnew: false, message: 'Lanjutkan Input OTP Anda yg dikirim ke nomor '+item.phone, dataobject: item, timetoresend: (3 - getRegisterTimeDurationInMinute(item.registertime)) })
                } else {
                    console.log('otp re-generated')
                    //bila status = verifying dan registertime > 4 menit, generate ulang dan kirim OTP, update register time jg di db
                    const otp = generateOtp()  
                    collectionUser.findOneAndUpdate(
                        { name },
                        { $set: {registertime: new Date(), otp} },
                        {
                            upsert: true, 
                            returnNewDocument: true
                        },
                        function(err, result) {
                            if (err) {
                                return res.status(500).send('Gagal Insert / update Record')
                            } else {
                                //KIRIM SMS RESEND OTP
                                ZenzivaSMSSender.sendSMS(
                                    result.value.phone, SMS_OTP_RESEND_TEMPLATE(otp), (error, response) => {
                                        if (error) {
                                            console.log(error)
                                            return res.status(500).send('kirim sms error, cek log')
                                        }
                                        //sms berhasil 
                                        console.log(response)
                                    }
                                )
                            
                                res.send({isnew: false, message: 'OTP Sedang dikirimkan ke nomor: '+result.value.phone, dataobject: result.value, timetoresend: 3})
                            }
                        }
                    )
                }

                return
            }
            res.send('data tidak ditemukan')
        })


    })

})

function getRegisterTimeDurationInMinute(startTime) {
    return Math.round(moment.duration(moment().diff(moment(startTime))).asMinutes())
}

function generateOtp() {
    let result = Math.round(Math.random() * 999999)
    result = (result < 100000) ? result+'0' : result+''
    return result
}