const axios = require('axios') 
// const https = require('https')
let setting = null

const URL_API_SMS = 'https://gsm.zenziva.net/api/sendsms/' //'https://gsm.zenziva.net/api/sendOTP/'

module.exports = {
    Config: function (settingIn) {
        setting = settingIn 
    },
    getConfig: function() {
        return setting
    },
    sendSMS: function(targetno, pesan, callback) {
        if (!setting) return 'Setting Belum di set'

        axios.post(URL_API_SMS, {
            userkey: setting.username,
            passkey: setting.password,
            nohp: targetno,
            pesan: pesan
        }).then(function (response) { 
            callback(null, response.data)
        }).catch(function (error) {
            // handle error
            callback(error, null)
        })
 
    }
}
