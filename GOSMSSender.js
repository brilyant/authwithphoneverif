const axios = require('axios')
const md5 = require('md5')
const https = require('https')
let setting = null

const URL_API_SMS = function (setting, targetno,text) {
    const auth = md5(setting.username+setting.password+targetno)
    return `${setting.servername}/sendSMS.php?username=${setting.username}&mobile=${targetno}&message=${text}&auth=${auth}`
}

module.exports = {
    Config: function (settingIn) {
        setting = settingIn 
    },
    getConfig: function() {
        return setting
    },
    sendSMS: function(targetno, text, callback) {
        if (!setting) return 'Setting Belum di set'
        const urlSMS = URL_API_SMS(setting, targetno, text)

        axios.get(urlSMS, {
            proxy: false,
            httpsAgent: https.Agent({
                rejectUnauthorized: false // Allows the use of self-signed certificates (not recommended)
            })
        }).then(function (response) { 
            callback(null, response.data)
        }).catch(function (error) {
            // handle error
            callback(error, null)
        })
 
    }
}
